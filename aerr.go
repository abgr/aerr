package aerr

//TODO update docs
type aerrErrorWithCode struct {
	error
	errorCode int
}

type ErrorCode interface {
	error
	Code() int
}

func (e aerrErrorWithCode) Code() int {
	return e.errorCode
}

type aerrError struct {
	errorString string
}

func (err *aerrError) Error() string {
	return err.errorString
}

//New is built-in errros.New
func New(text string) error {
	return &aerrError{text}
}

func NewErrorCode(text string, code int) ErrorCode {
	return aerrErrorWithCode{New(text), code}
}
func WrapWithCode(e error, code int) ErrorCode {
	return aerrErrorWithCode{e, code}
}
func SetErrorCode(e *error, code int) {
	*e = aerrErrorWithCode{*e, code}
}
func GetErrorCode(e error) int {
	return e.(ErrorCode).Code()
}
func HasErrorCode(e error) bool {
	if _, err := GuardWithArgs(getErrorCode, e); err == nil {
		return true
	}
	return false
}
func getErrorCode(e ...interface{}) interface{} {
	e[0].(ErrorCode).Code()
	return nil
}
func Guard(unsafeFunc func()) (recoverdError interface{}) {
	guard(unsafeFunc, &recoverdError)
	return
}
func guard(unsafeFunc func(), recovered *interface{}) {
	defer guardDefer(recovered)
	unsafeFunc()
}
func GuardWithArgs(unsafeFunc func(...interface{}) interface{}, args ...interface{}) (returnedValue interface{}, recoverdError interface{}) {
	returnedValue = guardWithArgs(unsafeFunc, &recoverdError, &args)
	return
}
func guardWithArgs(unsafeFunc func(...interface{}) interface{}, recovered *interface{}, args *[]interface{}) interface{} {
	defer guardDefer(recovered)
	return unsafeFunc(*args...)
}
func guardDefer(recovered *interface{}) {
	*recovered = recover()
}

//Catch recover the panics and act like a gaurd to prevent the carches.
//if the recovered is not nil (means there must be an non-nil paniced value) and the "handler" is not nil too, call the handler with the recovered (paniced) value.
//a "catch" like mechanism for Go
func Catch(handler func(interface{})) {
	recovered := recover()
	if recovered != nil && handler != nil {
		handler(recovered)
	}
}

//Ignore ignores paniced error
func Ignore() {
	recover()
}

func IsError(value interface{}) bool {

	if Guard(func() {
		_ = value.(error)
	}) == nil {
		return true
	}
	return false
}

func IsErrorCode(value interface{}) bool {
	if Guard(func() {
		_ = value.(ErrorCode)
	}) == nil {
		return true
	}
	return false
}

//Panicerr checks the "condition" to be nil else panics the "alternative" is not nil else panics the "condition" itself.
//a "throw" like mechanism for Go
func Panicerr(condition interface{}, alternative interface{}) {
	if condition != nil {
		if alternative == nil {
			panic(condition)
		} else {
			panic(alternative)
		}
	}
}

//IsErrAssertion checks the "err" is an assertion error.
func IsErrAssertion(err error) bool {
	return err.Error() == "ErrAssertion"
}

//ErrAssertion is an assertion error
var ErrAssertion = NewErrorCode("ErrAssertion", -1)

//Assert checks the "condition" to be true else panics the "err".
func Assert(condition bool, err interface{}) {
	if !condition {
		if err == nil {
			panic(ErrAssertion)
		} else {
			panic(err)
		}
	}
}
