**Aerr** (Ar)
A tiny and simple but useful go packge.
It's contains two methods for a throw-catch like error handling mechanism for Go.
Use defer Ignore({handler?}) for catching and Panicerr({error?}) for throwing.
```
func PrintPanicedWithoutCrash(e interface{}){
    fmt.Println(e)
}
func aFuncThatMayHasErrorsAndPanics(){
    defer Catch(PrintPanicedWithoutCrash)
    thereIsSomeNonNilError := errors.New("somewhat")
    //without need to write an ugly if like this
    ////if thereIsSomeNonNilError != nil {
    ////    panic(thereIsSomeNonNilError)
    ////}
    //just need to pass it to the Panicerr
    Panicerr(thereIsSomeNonNilError)
}
```